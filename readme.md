***
# Roothaan-Hartree-Fock atomic data-sets and momentum distributions

## author: Aaron D. Kaplan

***

### Code strucutre:

  orbs.py : contains primary routines. Can construct spin-density variables (n_sigma, |grad n_sigma|, laplacian n_sigma, tau) using RHF data set entries, and momentum distributions

  data_sets.py : contains RHF tabulated Slater orbital basis set data from the older Clementi and Roetti (1974) set and new Bunge, Barrientos, and Bunge (1993) data set. Citations are given in the code, search for "doi" to get a hit for citations.

  plot_fft_dens.py : utility for plotting momentum distributions

***

### Directory structure:

  You don't need to set this up in advance; running any of the Python scripts that make use of a given directory will create them as needed.

  data_files : stores all outputs of orbs.py
  plots : stores all outputs of plots.py
