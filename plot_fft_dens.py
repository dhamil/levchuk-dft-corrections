import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)
from os import system,path

out_dir = './plots/'
for adir in [out_dir]:
    if not path.isdir(adir):
        system(('mkdir {:}').format(adir))

def cdf_plot(dset):

    fig,ax = plt.subplots(figsize=(6,4))

    p,n_unp,n_pol = np.transpose(np.genfromtxt('./data_files/Fe_'+dset+'_fft_sph_cdf.csv',delimiter=',',skip_header=1))
    ax.plot(p,n_unp,color = 'darkblue',label='Unpolarized',linewidth=1.5)
    ax.annotate('Unpolarized',(29,0.65),color='darkblue',fontsize=12)
    ax.plot(p,n_pol,color = 'darkorange',label='Polarized',linestyle='--',\
        linewidth=1.5)
    ax.annotate('Polarized',(33,0.92),color='darkorange',fontsize=12)
    ax.set_xlabel('$p$ (keV/c)',fontsize=12)
    ax.set_xlim([0.0,50])
    #ax.set_xscale('log')
    ax.set_ylim([0.0,1.0])
    ax.set_ylabel('CDF',fontsize=12)
    ax.tick_params(axis='both',labelsize=12)
    #ax.legend(fontsize=12)

    ax.xaxis.set_minor_locator(MultipleLocator(2.0))
    ax.xaxis.set_major_locator(MultipleLocator(10.0))
    ax.yaxis.set_minor_locator(MultipleLocator(0.05))
    ax.yaxis.set_major_locator(MultipleLocator(0.2))

    ax.tick_params(which='major',direction='in')
    ax.tick_params(which='minor',direction='in')

    plt.title('Spherically-averaged momentum CDFs',fontsize=12)
    #plt.show() ; exit()
    plt.savefig(out_dir+'Fe_'+dset+'_cdfs.pdf',dpi=600,bbox_inches='tight')
    return

def pdf_plot(dset):

    fig,ax = plt.subplots(figsize=(6,4))

    p,n_unp,n_pol = np.transpose(np.genfromtxt('./data_files/Fe_'+dset+'_fft_sph.csv',delimiter=',',skip_header=1))

    ax.plot(p,n_unp,color = 'darkblue',label='Unpolarized',linewidth=1.5)
    if dset == 'CR74':
        upos = (.26,0.01)
    elif dset == 'BBB93':
        upos = (.06,0.02)
    ax.annotate('Unpolarized',upos,color='darkblue',fontsize=12)

    ax.plot(p,n_pol,color = 'darkorange',label='Polarized',linestyle='--', \
        linewidth=1.5)
    if dset == 'CR74':
        ppos = (1.7,0.13)
    elif dset == 'BBB93':
        ppos = (1.2,.07)
    ax.annotate('Polarized',ppos,color='darkorange',fontsize=12)

    ax.set_ylabel('PDF',fontsize=12)
    ax.set_xlim([1.e-2,150])
    ax.set_xscale('log')
    ax.set_ylim([0.0,ax.get_ylim()[1]])
    ax.set_xlabel('$p$ (keV/c)',fontsize=12)
    ax.tick_params(axis='both',labelsize=12)
    #ax.legend(fontsize=12)

    if dset == 'CR74':
        ymint = 0.01
        ymajt = 0.05
    elif dset == 'BBB93':
        ymint = 0.005
        ymajt = 0.02

    ax.yaxis.set_minor_locator(MultipleLocator(ymint))
    ax.yaxis.set_major_locator(MultipleLocator(ymajt))

    ax.tick_params(which='major',direction='in')
    ax.tick_params(which='minor',direction='in')

    fig.suptitle('Spherically-averaged momentum PDFs',fontsize=12)
    #plt.show() ; exit()
    plt.savefig(out_dir+'Fe_'+dset+'_pdfs.pdf',dpi=600,bbox_inches='tight')

    plt.cla()
    plt.clf()
    plt.close()

    return

def pdf_plot_2_panel(dset):

    fig,ax = plt.subplots(1,2,figsize=(16,8))

    p,n_unp,n_pol = np.transpose(np.genfromtxt('./data_files/Fe_'+dset+'_fft_sph.csv',delimiter=',',skip_header=1))

    ax[1].semilogy(p,n_unp,color = 'tab:blue',label='Unpolarized')
    ax[1].semilogy(p,n_pol,color = 'tab:orange',label='Polarized')
    ax[1].set_yscale("log", nonposy='clip')
    ax[0].plot(p,n_unp,color = 'tab:blue',label='Unpolarized')
    ax[0].plot(p,n_pol,color = 'tab:orange',label='Polarized')
    ax[0].set_ylabel('PDF',fontsize=20)
    ax[0].set_xlim([0.0,20])
    ax[0].set_ylim([0.0,ax[0].get_ylim()[1]])
    ax[1].set_xlim([0.0,p.max()])
    for i in range(2):
        #ax[i].annotate('Unpolarized',(10,0.06),color='tab:blue',fontsize=16)
        #ax[i].annotate('Polarized',(3,.15),color='tab:orange',fontsize=16)
        ax[i].set_xlabel('$p$ (keV/c)',fontsize=20)
        ax[i].tick_params(axis='both',labelsize=16)
    ax[0].legend(fontsize=16)

    fig.suptitle('Spherically-averaged momentum PDFs',fontsize=20)
    plt.savefig(out_dir+'Fe_'+dset+'_pdfs_2_panel.pdf',dpi=600,bbox_inches='tight')

    plt.cla()
    plt.clf()
    plt.close()

    return

if __name__ == "__main__":

    for set in ['CR74','BBB93']:
        cdf_plot(set)
        pdf_plot(set)
