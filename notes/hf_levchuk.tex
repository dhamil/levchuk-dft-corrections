\documentclass[12pt]{article}
\usepackage[margin=0.75in]{geometry}

\usepackage[T1]{fontenc}
\usepackage{palatino}

\usepackage{amsmath,graphicx,bm,bibentry,tocbasic,etoolbox,authblk,mathtools}
\graphicspath{{../plots/}}
\usepackage[style=numeric,backend=biber,sorting=none,style=phys]{biblatex}
\addbibresource{hf_levchuk.bib}

\DeclareCiteCommand{\citenum}
  {}
  {\printfield{labelnumber}}
  {}
  {}

\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
\newcommand{\br}{\bm{r}}

\begin{document}

\noindent{\bf \large Notes on Hartree-Fock momentum distributions}\\
\noindent{\bf Aaron D. Kaplan, \today}

\section{Hartree-Fock method}

All non-relativistic theories of many electrons in the presence of a single nucleus seek to solve $N$ coupled equations of motion of the form
\begin{equation}
  \hat{H}\Psi(\br_1 \sigma_1,...,\br_N\sigma_N) = \sum_{i=1}^N \left\{ -\frac{\hbar^2}{2m_{\mathrm{e}}}\nabla_i^2 - \frac{Z e^2}{r_i} + \sum_{j > i}\frac{e^2}{|\br_i - \br_j|} - \varepsilon_i \right\}\Psi(\br_1 \sigma_1,...,\br_N\sigma_N) = 0. \label{eq:hamil}
\end{equation}
The primary methods of solution are wavefunction theories, where the many-body wavefunction $\Psi$ is approximated as a linear combination of product states or configurations (products of antisymmetrized product states), or density-functional methods, where the total electron (spin-) densities are calculated.

Unrestricted Hatree-Fock (HF) theory is an approximate theory that assumes the total electronic wavefunction is a single Slater determinant of one-electron states $\phi_{i,\sigma}(\br)$, where $i$ labels the particle state and $\sigma$ the spin state ($\pm \hbar/2$ along the $\hat{z}$ axis). It is readily shown that the Hartree-Fock ansatz for $\Psi$ yields $N$ coupled equations of the form
\begin{eqnarray}
 & &\left[ - \frac{\hbar^2}{2m_{\mathrm{e}}}\nabla_i^2 - \frac{Z e^2}{r_i} + e^2\sum_{j}\int\frac{|\phi_j(\br')|^2}{|\br - \br'|} d^3 r'- \varepsilon_i \right] \phi_{i,\sigma}(\br) \nonumber \\
 & & - \delta_{\sigma,\sigma'}e^2 \sum_{j\neq i}\int \frac{\phi_{j,\sigma'}^*(\br')\phi_{i,\sigma'}(\br')\phi_{j,\sigma}(\br)}{|\br - \br'|} d^3 r' = 0. \label{eq:hamil_hf}
\end{eqnarray}
For open-shell atoms, like Fe, an unrestricted solution, where the number of spin-up and spin-down electrons are unequal, is necessary for a semi-accurate description. The alternative is the restricted HF solution, which is only applicable to closed shell systems (e.g., rare gas atoms).

Unlike more modern theories of many-electrons, like Kohn-Sham density functional theory (DFT), the HF solution neglects ``electron-electron correlations.'' The beyond-HF solution accounts for correlation either exactly (by solving Eq. \ref{eq:hamil} directly) or approximately via perturbation theory-like methods (configuration interaction, coupled cluster, etc.). For atoms and small molecules, it's now possible to exactly solve the Kohn-Sham equations \cite{ospadov2018}, though typically approximations are made (and are necessary for calculations of periodic solids where $N \to \infty$ unless isolated to a periodic unit cell).

DFT methods are easily generalized to systems with non-integral occupancy (e.g., finite temperature distributions, systems with degenerate ground-states like Jahn-Teller distorted molecules, and the anti-/ferromagnetic ground states of transition metals), whereas HF and beyond-HF theories often become intractable for fractional occupancy. Thus ferromagnetic Fe, which has a magnetic moment per atom of roughly 2.1 $\mu_{\mathrm{B}}$, is underestimated by the HF solution (2 $\mu_{\mathrm{B}}$), whereas simple approximations in density functional theory yield essentially exact results \cite{ekholm2018}.

\section{Basis sets and spherical averages}

The solutions presented here solve the HF equations using Slater-type one-electron wavefunctions (orbitals) as the basis set. This method, which transforms the HF equations into a generalized eigenvalue problem, are often called Roothaan-Hartree-Fock (RHF) methods \cite{clementi1974}. Slater wavefunctions are orthonormal and mimic the cusp near the nucleus seen in exact solutions for $\Psi$,
\begin{eqnarray}
  \chi_{\zeta n\ell m}(r,\theta,\phi) &=& \chi_{\zeta n }(r) ~Y_{\ell m}(\theta,\phi), \\
  \chi_{\zeta n }(r) &=& \frac{(2 \zeta)^{n+1/2}}{(2n)!} r^{n-1} e^{-\zeta r}
\end{eqnarray}
with $Y_{\ell m}$ spherical harmonics. Note that the quantities are dimensionless in this equation: $r$ is measured in units of bohr ($a_0 = \hbar^2/(m_e e^2)\approx 0.529$ \AA{}) and thus momentum in $\hbar/a_0$. The $\zeta$ parameters resemble effective nuclear charges, however they're typically determined variationally and have no physical interpretation. Moreover, Slater wavefunctions have known Fourier transforms \cite{belki1989}
\begin{eqnarray}
  \chi_{\zeta n\ell m}(k,\theta_k,\phi_k) &=& \chi_{\zeta n \ell}(k) Y_{\ell m}(\theta_k,\phi_k), \\
  \chi_{\zeta n \ell}(k) & = & \frac{2^{n-1}(n-\ell)!}{\pi^2}(i k)^{\ell}\frac{\zeta^{n- \ell} (2\zeta)^{n+1/2}}{[(2n)!]^{1/2}}\sum_{j=0}^{\lfloor (n- \ell)/2 \rfloor}  \left\{ \frac{\omega_j^{n\ell}}{(k^2 + \zeta^2)^{n+1-j}} \right\} \\
  \omega_0^{n\ell} &=& \frac{n!}{(n-\ell)!} \\
  \omega_j^{n\ell} &=& -\frac{1}{4\zeta^2}\frac{(n - \ell - 2j +  1)(n - \ell - 2j +2)}{j(n - j + 1)}\omega_{j-1}^{n\ell}, \qquad j >0.
\end{eqnarray}
$i$ is the imaginary unit and $\lfloor M \rfloor$ indicates the floor function (greatest integer less than or equal to $M$).

The RHF shells are represented as linear combinations of Slater wavefunctions
\begin{equation}
  \phi_{n\ell}(\br) = Y_{\ell 0}(\theta, \phi) \sum_{n,\ell} C_{n,\ell} \chi_{\zeta n}(r),
\end{equation}
where $m=0$ to make the solution tractable and the angular dependence, identical in each term, has been factored out. $n$ and $\ell$ may be used to label the shell as is done with hydrogenic wavefunctions (e.g., $n=1$, $\ell=0$ would be a 1s shell, $n=3$, $\ell=1$ would be a 3p shell). Consequently, the Fourier transform of each shell also has the same structure
\begin{equation}
  \phi_{n\ell}(k,\theta_k,\phi_k) = Y_{\ell 0}(\theta_k, \phi_k)\sum_{n,\ell} C_{n,\ell} \chi_{\zeta n\ell}(k).
\end{equation}
Suppose each shell contains $f_{n\ell,\mathrm{unp.}}$ paired (or unpolarized) and $f_{n\ell,\mathrm{pol.}}$ unpaired (or polarized) electrons. Then the momentum distributions may be written as
\begin{equation}
  n_{S}(\bm{k}) = \sum_{n\ell} f_{n\ell,S} |\phi_{n\ell}(k,\theta_k,\phi_k)|^2 \label{eq:mom_distr}
\end{equation}
where $S=$ unp. or pol. As we're only interested in spherical averages of the momentum distributions, define a spherical average as
\begin{equation}
  \langle X(r,\theta,\phi) \rangle_{\mathrm{sph}} = \frac{1}{4\pi}\int_0^{2\pi} d\phi \int_{-1}^1 d(\cos\theta) X(r,\theta,\phi).
\end{equation}
From the form of Eq. \ref{eq:mom_distr}, we need only consider the spherical average of the square magnitude of each shell. The only angular dependence in $|\phi_{n\ell}(k,\theta_k,\phi_k)|^2$ is through the spherical harmonic, which averages out to unity
\begin{eqnarray}
  \langle |\phi_{n\ell}(k,\theta_k,\phi_k)|^2 \rangle_{\mathrm{sph}} &=& \frac{1}{4\pi} \left|\sum_{n,\ell} C_{n,\ell} \chi_{\zeta n \ell}(k)\right|^2 \int_0^{2\pi} d\phi_k \int_{-1}^1 d(\cos\theta_k) |Y_{\ell 0}(\theta_k,\phi_k)|^2 \\
  \langle |\phi_{n\ell}(k,\theta_k,\phi_k)|^2 \rangle_{\mathrm{sph}} &=& \frac{1}{4\pi} \left|\sum_{n,\ell} C_{n,\ell} \chi_{\zeta n \ell}(k)\right|^2
\end{eqnarray}
using the fact that each shell has the same angular character. Alternatively, as is done in the code, we can define a spherically-averaged shell
\begin{equation}
  \widetilde{\phi}_{n\ell}(k) = \frac{1}{\sqrt{4\pi}}\sum_{n,\ell} C_{n,\ell} \chi_{\zeta n\ell}(k)
\end{equation}
which yields the same momentum distribution.

Reference \citenum{clementi1974} was a landmark work that tabulated the self-consistent $\zeta$, $C_{n,\ell}$, and $f_{n\ell,S}$ parameters for all neutral atoms up to Xe. More modern data sets \cite{bunge1993} exist that correct the parameters in their work, however the difference in total energies for Fe is $\sim 0.005\%$ different in the two works, and the densities can be expected to be similar.

\section{Momentum distributions}

Suppose we have a self-consistent solution for $\Psi$, and we know that $N_{\uparrow}$ ($N_{\downarrow}$) electrons have spin up (down). If we assume that the response of the system is purely ferromagnetic in character (as it would be for magnetized Fe), then the number of unpolarized (paired) electrons is $N_{\mathrm{unp.}} = 2 N_{\downarrow}$, and the number of polarized (unpaired) electrons is $N_{\mathrm{pol.}} =N_{\uparrow} - N_{\downarrow}$. As needed by conservation of particle number, $N_{\mathrm{unp.}}+N_{\mathrm{pol.}} = N$.

This is because the RHF solution cannot account for partial occupancy of spin states (or arbitrary spin directions), so the response is either in the direction of the field (up), or opposing it (down). Thus to determine the momentum distribution, we need to know the occupancy $f_{n \ell,\sigma}$ of each shell for spin-up and spin-down electrons. As expected, $ f_{n1,\sigma} = 0,1$, $f_{n2,\sigma} = 0,1,2,3$, $f_{n3,\sigma} = 0,1,2,3,4,5$, etc. for each spin $\sigma$.

The solution contains this information, and thus we can write the spherically-averaged momentum distributions as
\begin{eqnarray}
  n_{\mathrm{unp.}}(p) &=& \sum_{n \ell}  2 f_{n \ell,\downarrow} |\widetilde{\phi}_{n\ell}(p/\hbar)|^2\\
  n_{\mathrm{pol.}}(p) &=& \sum_{n \ell} (f_{n \ell,\uparrow}-f_{n \ell,\downarrow}) |\widetilde{\phi}_{n\ell}(p/\hbar)|^2,
\end{eqnarray}
with $p =\hbar k$. The momentum-density probability density functions (PDF) are then
\begin{equation}
  n^{\mathrm{PDF}}_{S}(p) = \frac{ 4\pi p^2 n_{S}(p)}{4\pi \int_0^{\infty} p^2 n_{S}(p) dp},
\end{equation}
and the cumulative distribution functions (CDF) are defined as
\begin{equation}
  n^{\mathrm{CDF}}_{S}(p) = 4\pi \int_0^p  n^{\mathrm{PDF}}_{S}(p') p'^2 dp',
\end{equation}
where $S =$ unp. or pol.

The calculations so far have used the first table for Fe listed in Ref. \citenum{clementi1974} on p. 207 (compare this to the updated table on p. 134 of Ref. \citenum{bunge1993}). This is the well-known 1s$^2$2s$^2$2p$^6$3s$^2$3p$^6$4s$^1$3d$^7$ configuration of Fe, however both Refs. \citenum{clementi1974} and \citenum{bunge1993} find the ground-state to be a spin-quintuplet (four unpaired electrons). The configuration of the spin-quintuplet Fe atom given in Ref. \citenum{bunge1993} has a completed 4s shell, 1s$^2$2s$^2$2p$^6$3s$^2$3p$^6$4s$^2$3d$^6$. In the code, the data sets are identified by the authors' initials and the last two years of publication: ``CR74'' \cite{clementi1974} and ``BBB93'' \cite{bunge1993}.

It should be noted that because the basis set in the RHF method is truncated at finite order, multiple solutions of different character can emerge as degenerate or nearly degenerate ground states. There are a few solutions given for Fe on pp. 207 -- 211 that are nearly degenerate. However, the configuration used has the lowest total energy, and can be taken to be the ground state.

\section{Differences between HF and hydrogenic solutions}

This section highlights what HF theory and the hydrogenic approximation miss in the full Hamiltonian, and shows how the HF solution will necessarily improve upon the hydrogenic one. In general, the true many-electron wavefunction cannot be written as a linear combination of product states, as a product state wavefunction can be exact only for a noninteracting system.

From Eq. \ref{eq:hamil_hf}, it can be seen that the HF kinetic energy density will lack a component due to the HF wavefunction being a summation of product states. This is called the interacting kinetic energy density. Moreover, the third (Hartree electrostatic repulsion) and fifth terms (exchange potential) on the LHS of Eq. \ref{eq:hamil_hf} overestimate the exact electron-electron repulsion term in Eq. \ref{eq:hamil}.

The Hartree potential, which is positive definite, greatly overestimates the total electron-electron repulsion. The exchange energy term, which is negative definite, reduces the overcorrection made by the Hartree potential. The residual error in the kinetic energy and electron-electron repulsion are generally lumped together as the non-positive (quantum chemical) correlation potential. Note also that the quantum chemical correlation energy is an upper bound to the DFT correlation energy.

The quantum chemical correlation potential and energy are generally greater in low-density regions. In high-density regions, correlation is dominated by the kinetic energy, and the HF approximation is not bad. Thus the HF wavefunction can be expected to be most accurate in the core region.

The hydrogenic solution assumes that the electrons are completely non-interacting,
\begin{equation}
  \left[ - \frac{\hbar^2}{2m_{\mathrm{e}}}\nabla_i^2 - \frac{Z e^2}{r_i} - \varepsilon_i \right] \phi_{i,\sigma}(\br) = 0
\end{equation}
and thus greatly underestimates the ground state energy, tending to localize the electron density. An alternative is to consider the limit of static, long-wavelength screening, motivated by the Thomas-Fermi approximation \cite{ashcroft_mermin}
\begin{equation}
  \left[ - \frac{\hbar^2}{2m_{\mathrm{e}}}\nabla_i^2 - \frac{Z e^2}{r_i} e^{-k_s r_i} - \varepsilon_i \right] \phi_{i,\sigma}(\br) = 0
\end{equation}
where $k_s$ is a characteristic screening length. This approximation may yield some correct qualitative features of the system (e.g., the general spin-density shape), but represents an uncontrolled approximation, as $k_s$ is completely empirical. The linear response of an atom to a perturbation of frequency $\omega$ and wavevector $\bm{q}$ is dominated by electron-electron interactions, and shows characteristic structure in its quasiparticle and excited state spectra (resonances, lifetimes, etc.) that are missed by the static ($\omega \to 0$), long-wavelength ($|\bm{q}|\to 0$) limit of the linear response.

The hydrogenic solution is the ground state of no physical many-electron system \cite{kaplan2020} and violates the spin-statistics theorem (a single product state wavefunction cannot be antisymmetric under interchange of particle indices). The Hartree solution (setting the exchange term to zero and using only a single product state wavefunction) systematically improves upon the hydrogenic solution, and the HF solution further improves upon the Hartree solution by using an antisymmetric many-electron wavefunction.

\clearpage

\section{Plots of the momentum PDFs and CDFs}

\subsection{CR74 dataset}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{Fe_CR74_pdfs.pdf}
  \caption{Momentum PDF using the dataset of Ref. \citenum{clementi1974}.}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{Fe_CR74_cdfs.pdf}
  \caption{Momentum CDF using the dataset of Ref. \citenum{clementi1974}.}
\end{figure}

\clearpage

\subsection{BBB93 dataset}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{Fe_BBB93_pdfs.pdf}
  \caption{Momentum PDF using the dataset of Ref. \citenum{bunge1993}.}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{Fe_BBB93_cdfs.pdf}
  \caption{Momentum CDF using the dataset of Ref. \citenum{bunge1993}.}
\end{figure}

\clearpage

\printbibliography

\end{document}
